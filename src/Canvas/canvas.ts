import { createCanvas, loadImage } from 'canvas';
import { TestElement,Location } from 'web-vitals-test/lib/types';

export class Canvas {
  static async drawRectangle(image: string, location: Location) {
    const img = await loadImage(image);

    const canvas = createCanvas(img.width, img.height);
    const ctx = canvas.getContext('2d');

    ctx.drawImage(img, 0, 0);

    ctx.beginPath();
    ctx.lineWidth = 10;
    ctx.setLineDash([6]);
    ctx.strokeStyle = 'red';
    ctx.rect(location.x, location.y, location.width, location.height);
    ctx.stroke();
    ctx.closePath();

    return canvas.toBuffer();
  }

  static async drawRectangleCLS(image: string, elements: TestElement[]) {
    const img = await loadImage(image);

    const canvas = createCanvas(img.width, img.height);
    const ctx = canvas.getContext('2d');

    ctx.drawImage(img, 0, 0);

    for (const element of elements) {
      const newLocation = element.newLocation;
      if (newLocation) {
        ctx.beginPath();
        ctx.lineWidth = 10;
        ctx.strokeStyle = 'red';
        ctx.rect(newLocation.x, newLocation.y, newLocation.width, newLocation.height);
        ctx.stroke();
        ctx.closePath();
      }

      const oldLocation = element.previousRect;
      if (oldLocation?.x) {
        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.setLineDash([6]);
        ctx.strokeStyle = 'red';
        ctx.rect(oldLocation.x, oldLocation.y, oldLocation.width, oldLocation.height);
        ctx.stroke();
        ctx.closePath();
      }
    }

    return canvas.toBuffer();
  }

  static async reducesize(image: string) {
    const img = await loadImage(image);
    let canvas;
    let ctx;
    if (img.height < img.width) {
      canvas = createCanvas(290, 290 * (img.height / img.width));
      ctx = canvas.getContext('2d');
      ctx.drawImage(img, 0, 0, 290, 290 * (img.height / img.width));
    } else {
      canvas = createCanvas(290 * (img.width / img.height), 290);
      ctx = canvas.getContext('2d');
      ctx.drawImage(img, 0, 0, 290 * (img.width / img.height), 290);
    }

    return canvas.toBuffer();
  }
}
