import { WebVitalsTestScreenshot as WebVitalsTest } from './../index';

describe('puppeteer integration', function () {
  it('End to End - Desktop', async () => {
    let result = await WebVitalsTest.goTo_ClickAndMeasure(
      'http://automationpractice.com/',
      'InitialLoad',
      { selector: '[id="search_query_top"]' },
      { waitFor: 0 },
    );

    console.log(JSON.stringify(result, null, 2));

    let scrollresult = await WebVitalsTest.scrollAndMeasure('ScrollDown', { down: 1000, right: 0 });

    await WebVitalsTest.scrollAndMeasure('ScrollUp', { down: -1000, right: 0 });

    console.log('test is completed');
  });

  it('End to End - mobile', async () => {
    const page = WebVitalsTest.getPage();
    let result = await WebVitalsTest.goToAndMeasure('http://automationpractice.com/', 'InitialLoad', {
      waitUntil: 'networkidle2',
    });

    console.log(JSON.stringify(result, null, 2));

    await WebVitalsTest.clickAndMeasure({ selector: '[id="search_query_top"]' }, 'ClickElement');

    let scrollresult = await WebVitalsTest.scrollAndMeasure('ScrollDown', { down: 1000, right: 0 });

    await WebVitalsTest.scrollAndMeasure('ScrollUp', { down: -1000, right: 0 });
  });
});
