import { Browser } from 'puppeteer-core';
import { WebVitalsTestScreenshot as WebVitalsTest } from './../index';
import * as puppeteer from 'puppeteer-core';

let browser: unknown;
before(function () {
  WebVitalsTest.init({ port: 3001, mode: process.env.MODE || 'full' });
});

after(function () {
  WebVitalsTest.stopServer();
});

beforeEach(async function () {
  browser = await puppeteer.launch({
    executablePath:'/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    headless: true,
    defaultViewport: { height: 1200, width: 1950 },
    args: ['--start-maximized'],
  });
  await WebVitalsTest.setBrowser(browser as Browser);
});

afterEach(function () {
  (browser as Browser).close();
});
