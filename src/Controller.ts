import {Controller as WebVitalsTest} from 'web-vitals-test/lib/Controller'
import { CLS, FID, LCP,Location } from 'web-vitals-test/lib/types';
import { Canvas } from './Canvas/canvas';
import * as fs from 'fs'

export class Controller extends WebVitalsTest{
    constructor(){
       super()
       super.beforeAction = this.beforeAction
       super.afterAction = this.afterAction

       if (!fs.existsSync('report')) fs.mkdirSync('report');
       if (!fs.existsSync('report/imgs')) fs.mkdirSync('report/imgs');
   }

    async beforeAction(actionName:string,options: { beforeScreenshot: boolean; mode?: string }) {
        if (!this.cPage) throw new Error('Controller.setPage() method must be called before any Measure methods');
        if (!(await this.setNextAction(actionName)))
          throw new Error('Can not set next action. Check local server is running');
    if(options.beforeScreenshot){
            const currentScenarioIndex = this.reporter.getCurrentIndex();
            if (!fs.existsSync('report/imgs/' + currentScenarioIndex )) fs.mkdirSync('report/imgs/' + currentScenarioIndex );
            const beforeScreenshot = './report/imgs/' + currentScenarioIndex + '/before' + actionName + '.png';
            await this.cPage?.screenshot({
              path: beforeScreenshot,
            });
        }
    }
    
    async afterAction(
        actionName: string,
        internalEntry: string,
        options?: { getCLS?: boolean; getLCP?: boolean; getFID?: boolean; getNavigation?: boolean; mode?: string },
      ) {
        let afterScreenShot = '';
        let navigationTiming;
        const currentScenarioIndex = this.reporter.getCurrentIndex();
        if (!fs.existsSync('report/imgs/' + currentScenarioIndex )) fs.mkdirSync('report/imgs/' + currentScenarioIndex );
        afterScreenShot = './report/imgs/' + currentScenarioIndex + '/after' + actionName + '.png';
        await this.cPage?.screenshot({
          path: afterScreenShot,
        });
        navigationTiming = await this.getPerformanceTiming();
     
        const clsData: CLS | undefined = options?.getCLS ? await this.getCLS(actionName) : undefined;
        const lcpData: LCP | undefined = options?.getLCP ? await this.getLCP(actionName) : undefined;
        const fidData: FID | undefined = options?.getFID ? await this.getFID(actionName) : undefined;

        const currentURL = this.cPage?.url();

      await this.cPage?.screenshot({ path: afterScreenShot });

      
      const imgLocation = 'report/imgs/' + currentScenarioIndex+ '/';
      if(clsData){
      if(clsData?.elements && clsData.elements.length > 0){
          fs.writeFileSync(
            imgLocation + actionName + '_CLS.png',
            await Canvas.drawRectangleCLS(afterScreenShot, clsData.elements),
          );
      }
        else {
          fs.copyFileSync(afterScreenShot, imgLocation + actionName + '_CLS.png');
        }
        clsData.screenshot = imgLocation + actionName + '_CLS.png'
      }

      if(lcpData){
        if(lcpData?.element?.location){
            fs.writeFileSync(
              imgLocation + actionName + '_LCP.png',
              await Canvas.drawRectangle(afterScreenShot, lcpData.element.location),
            );
        }
        lcpData.screenshot = imgLocation + actionName + '_LCP.png'
        }

      await this.reporter.addEntry( {
        actionName,
        URL: currentURL,
        CLS: {
          value: clsData?.value || 0,
          elements: clsData?.elements || undefined,
          screenshot: clsData?.screenshot
        },
        FID: fidData ? { value: fidData.value, startTime: fidData.startTime } : undefined,
        LCP: lcpData ? { value: lcpData.value, element: lcpData?.element || undefined ,screenshot:lcpData?.screenshot} : undefined,
        navigationTimings: navigationTiming,
        screenshot: {
          before: './report/imgs/' + currentScenarioIndex + '/before' + actionName + '.png',
          after: afterScreenShot,
        },
      });
   

    return { CLS: clsData, FID: fidData, LCP: lcpData, navigation: navigationTiming };
    }
    
}